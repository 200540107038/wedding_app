import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wed_app/constant.dart';
import 'package:wed_app/screens/home_screen.dart';

class OprationScreen extends StatefulWidget {
  const OprationScreen({Key? key}) : super(key: key);

  @override
  State<OprationScreen> createState() => _OprationScreen();
}

class _OprationScreen extends State<OprationScreen> {
  int? selectedId;
  final textController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: kPrimaryColor,
          centerTitle: true,
          title: TextField(
            decoration:  InputDecoration(
              border: InputBorder.none,
              hintText: ' Text Here ',
              hintStyle: GoogleFonts.poppins(
                  fontSize: 20.0, color: Colors.white
              ),
            ),
            controller: textController,
          ),
          leading: IconButton(
            onPressed: () {},
            icon: SvgPicture.asset('assets/icons/menu.svg'),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            color: Colors.white70,
            // decoration: const BoxDecoration(
            //   gradient: LinearGradient(
            //       begin: Alignment.topCenter,
            //       end: Alignment.bottomCenter,
            //       colors: [Color.fromRGBO(97, 0, 255, 1), Color.fromRGBO(235, 92, 195, 1)]),
            // ),
            child: Center(

              child: FutureBuilder<List<Persons>>(
                  future: DatabaseHelper.instance.getPersons(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Persons>> snapshot) {
                    if (!snapshot.hasData) {
                      return Center(child: Text('Loading...'));
                    }
                    return snapshot.data!.isEmpty
                        ? Center(child: Text('No Persons in List.'))
                        : ListView(
                      children: snapshot.data!.map((persons) {
                        return Center(
                          child: Card(
                            color: selectedId == persons.id
                                ? Colors.white
                                : kPrimaryColor,
                            child: ListTile(
                              title: Text(persons.name,
                                style: GoogleFonts.poppins(
                                  fontSize: 20.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                ),),
                              textColor: Colors.white,
                              visualDensity: VisualDensity(vertical: 3,),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  IconButton(
                                      icon : Icon(Icons.edit),
                                      onPressed: () {
                                        setState(() {
                                          if (selectedId == null) {
                                            textController.text = persons.name;
                                            selectedId = persons.id;
                                          } else {
                                            textController.text = '';
                                            selectedId = null;
                                          }
                                        });
                                      }),
                                  IconButton(
                                    icon : Icon(Icons.delete),
                                    onPressed: () {
                                      setState(() {
                                        DatabaseHelper.instance.remove(persons.id!);
                                      }); },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    );
                  }),

            ),
          ),
        ],
      ),
      floatingActionButton: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: kPrimaryColor,
          shape:StadiumBorder(),
        ),
        child: Icon(Icons.add,),
        onPressed: () async {
          selectedId != null
              ? await DatabaseHelper.instance.update(
            Persons(id: selectedId, name: textController.text),
          )
              : await DatabaseHelper.instance.add(
            Persons(name: textController.text),
          );
          setState(() {
            textController.clear();
            selectedId = null;
          });
        },
      ),
    );
  }
}

class Persons {
  final int? id;
  final String name;

  Persons({this.id, required this.name});

  factory Persons.fromMap(Map<String, dynamic> json) => new Persons(
    id: json['id'],
    name: json['name'],
  );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
}

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'persons.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE persons(
          id INTEGER PRIMARY KEY,
          name TEXT
      )
      ''');
  }

  Future<List<Persons>> getPersons() async {
    Database db = await instance.database;
    var persons = await db.query('persons', orderBy: 'name');
    List<Persons> personsList = persons.isNotEmpty
        ? persons.map((c) => Persons.fromMap(c)).toList()
        : [];
    return personsList;
  }

  Future<int> add(Persons persons) async {
    Database db = await instance.database;
    return await db.insert('persons', persons.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('persons', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(Persons persons) async {
    Database db = await instance.database;
    return await db.update('persons', persons.toMap(),
        where: "id = ?", whereArgs: [persons.id]);
  }
}